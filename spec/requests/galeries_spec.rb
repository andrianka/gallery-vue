# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Gallery API', type: :request do
  let!(:galleries) { create_list(:gallery, 10) }
  let(:gallery_id) { galleries.first.id }
  let(:url) { Rails.application.credentials.tests[:url] }

  describe 'GET /galleries' do
    context 'without url params' do
      before { get '/galleries' }

      it 'returns all galleries' do
        expect(json).not_to be_empty
        expect(json.size).to eq(10)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
    context 'with url params' do
      before { get '/galleries', params: { gallery: { url: url } } }

      it 'returns all galleries' do
        expect(json).not_to be_empty
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'GET /galleries/:id' do
    before { get "/galleries/#{gallery_id}" }

    context 'when the record exists' do
      it 'returns the gallery' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(gallery_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:gallery_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Gallery/)
      end
    end
  end

  describe 'POST /galleries' do
    let(:valid_attributes) { { title: 'Gallery', website_url: url } }

    context 'when the request is valid' do
      before { post '/galleries', params: { gallery: valid_attributes } }

      it 'creates a gallery' do
        expect(json['title']).to eq('Gallery')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/galleries', params: { gallery: { title: 'Gallery' } } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Website url can't be blank/)
      end
    end
  end

  describe 'PUT /galleries/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }

    context 'when the record exists' do
      before { put "/galleries/#{gallery_id}", params: { gallery: valid_attributes } }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /galleries/:id' do
    before { delete "/galleries/#{gallery_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
