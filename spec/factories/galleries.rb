# frozen_string_literal: true

FactoryBot.define do
  factory :gallery do
    website_url { Faker::Internet.url }
    title { Faker::Game.title }
    last_update { '2020-10-29' }
  end
end
