# frozen_string_literal: true

RSpec.feature 'Images', type: :feature do
  describe 'with js', js: true do
    let(:url) { Rails.application.credentials.tests[:url] }

    scenario 'can see input for link' do
      visit '/'
      expect(page).to have_selector('input')
      expect(page).to have_field('url')
    end

    scenario 'can input link and get images' do
      visit '/'
      fill_in 'url', with: "#{url}\n"
      expect(page).to have_field('url', with: url)
      expect(page).to have_css('.v-image')
    end

    scenario 'can see modal slider with image' do
      visit '/'
      fill_in 'url', with: "#{url}\n"
      page.find(:css, '.v-image', match: :first).click
      expect(page).to have_css('.modal')
      expect(page).to have_selector('img')
      expect(page).to have_css("img[src*='#{url}']")
      assert have_no_field('url')
    end

    scenario 'can close modal with image' do
      visit '/'
      fill_in 'url', with: "#{url}\n"
      page.find(:css, '.v-image', match: :first).click
      page.find(:css, '.close', match: :first).click
      expect(page).to have_css('.v-image')
      expect(page).to have_field('url')
    end
  end
end
