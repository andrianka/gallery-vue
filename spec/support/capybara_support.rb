# frozen_string_literal: true

require 'capybara/rspec'

Capybara.register_driver :selenium_chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.javascript_driver = :selenium_chrome

Capybara.configure do |config|
  config.run_server = true
  config.ignore_hidden_elements = true
  config.default_max_wait_time = 3 # sec
end
