# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Gallery, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:website_url) }
    it { should allow_value('https://foo.com').for(:website_url) }
    it { should validate_length_of(:title).is_at_most(140) }
  end
end
