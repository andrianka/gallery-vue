# frozen_string_literal: true

class GalleriesController < ApplicationController
  before_action :set_gallery, only: %i[show update destroy]

  def index
    if params[:url]
      images = DownloadImagesService.call(params[:url])
    else
      galleries = Gallery.all
    end
    render json: images || galleries
  end

  def show
    json_response(@gallery)
  end

  def create
    @gallery = Gallery.create!(gallery_params)
    json_response(@gallery, :created)
  end

  def update
    @gallery.update(gallery_params)
    head :no_content
  end

  def destroy
    @gallery.destroy
    head :no_content
  end

  private

  def gallery_params
    params.require(:gallery).permit(:website_url, :title)
  end

  def set_gallery
    @gallery = Gallery.find(params[:id])
  end
end
