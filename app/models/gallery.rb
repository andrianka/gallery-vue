# frozen_string_literal: true

class Gallery < ApplicationRecord
  validates_presence_of :website_url
  validates_format_of :website_url, with: URI::DEFAULT_PARSER.make_regexp
  validates_length_of :title, maximum: 140
end
