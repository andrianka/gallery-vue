# frozen_string_literal: true

require 'open-uri'

class DownloadImagesService
  def self.call(*args, &block)
    new(*args, &block).execute
  end

  def initialize(url)
    @url = url
  end

  def execute
    data = []
    URI.open(@url) do |page|
      page_content = page.read
      links = page_content.scan(/href="([^"]*)/)
      links.each do |link|
        data.push(image_link(link[0])) if link_with_image?(link[0])
      end
    end
    data
  end

  private

  def image_link(link)
    uri = URI.parse(@url)
    "#{uri.scheme}://#{uri.host}#{link}"
  end

  def link_with_image?(link)
    link.ends_with?('jpg', 'png', 'jpeg', 'JPG', 'PNG', 'JPEG')
  end
end
