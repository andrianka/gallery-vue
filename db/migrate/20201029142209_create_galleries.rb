# frozen_string_literal: true

class CreateGalleries < ActiveRecord::Migration[6.0]
  def change
    create_table :galleries do |t|
      t.string :website_url
      t.string :title
      t.date :last_update

      t.timestamps
    end
  end
end
