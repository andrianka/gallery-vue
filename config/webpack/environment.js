const { environment } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')
const erb = require('./loaders/erb')
const vue = require('./loaders/vue')
const webpack = require('webpack')

environment.plugins.append('Provide', new webpack.ProvidePlugin({
  $: 'jquery',
  jquery: 'jquery',
  jQuery: 'jquery',
  'window.jQuery': 'jquery',
  Popper: ['popper.js', 'default'],
  moment: 'moment'
}))
environment.config.resolve.alias = { 'vue$': 'vue/dist/vue.esm.js' };
environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())
environment.loaders.prepend('vue', vue)
environment.loaders.prepend('erb', erb)
module.exports = environment
